<?php
require_once ("animal.php");

    class Ape extends Animal{
        public $get_name;
        public $get_legs = 2 ;
        public $get_cold_blooded = "True";
        
        public function __construct($get_name){
            $this->get_name = $get_name;
        }
        public function yell(){
            print("Auoo");
        }

    }
?>