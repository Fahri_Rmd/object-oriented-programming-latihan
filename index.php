<?php

require_once ("animal.php");
require_once ("Frog.php");
require_once ("Ape.php");

$sheep = new Animal ("shaun");

echo "Nama Hewan      = "  . $sheep->get_name ."<br>"; 
echo "Jumlah Kaki     = " . $sheep->get_legs ."<br>"; 
echo "Berdarah Dingin = " . $sheep->get_cold_blooded ."<br><br>";


$kodok = new Frog("buduk");

echo "Nama Hewan    = " . $kodok->get_name ."<br>";
echo "Jumlah Kaki   = " . $kodok->get_legs ."<br>";
echo "Berdarah Dingin = " . $kodok->get_cold_blooded ."<br>";
$kodok-> jump() ."<br>"; 

echo "<br> <br>";

$sungokong = new Ape("kera sakti");

echo "Nama Hewan    = " . $sungokong->get_name ."<br>";
echo "Jumlah Kaki   = " . $sungokong->get_legs ."<br>";
echo "Berdarah Dingin = " . $sungokong->get_cold_blooded ."<br>";
$sungokong->yell() ."<br>";

?>